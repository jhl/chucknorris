import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgForage } from 'ngforage';
import { Observable, BehaviorSubject, throwError, of } from 'rxjs';
import { map, mergeMap, flatMap, retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { Joke } from '../models/joke';
import { Favourite } from '../models/favourite';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public favorites$ = new BehaviorSubject<Favourite[]>([]);

  constructor(private http: HttpClient, private ngforage: NgForage) {
    this.ngforage.getItem<Favourite[]>('favourites')
      .then( (favourites) => {
        if (favourites) {
          this.favorites$.next(favourites);
        }
      });
  }

  getCategories (): Observable<string[]> {
    const url = environment.apiUrl.concat('/', 'jokes/categories');
    return this.http.get<string[]>(url);
  }

  getJokeByCategory (category: string): Observable<Joke> {
    const url = environment.apiUrl.concat('/', 'jokes/random');
    const params = new HttpParams().set('category', category);
    return this.http.get<Joke>(url, {params}).pipe(
      map( (joke) => {
        // fix jokes with empty category even though requested with one
        if (!joke.category || joke.category.length === 0) {
          joke.category = [category];
        }
        return joke;
      })
    );
  }

  getJokesByCategory (category: string, count: number): Observable<Joke[]> {
    const jokes: Joke[] = [];
    return this.getJokeByCategory(category)
    .pipe(
      mergeMap((res) => {
        // check existing jokes for id
        const found = jokes.filter(joke => joke.id === res.id);
        // unique id, add to array
        if (!found.length) {
          jokes.push(res);
        }
        // got enough jokes, return
        if (jokes.length === 3) {
          return of(jokes);
        } else {
          // stop eventually
          return throwError('not enough jokes found');
        }
      }),
      // retry 9 times to find jokes in case duplicates were found
      retry<Joke[]>(9)
    );
  }

  setFavourite(joke: Joke) {
    const faves = this.favorites$.value;
    if (faves.findIndex( item => item.id === joke.id) >= 0) {
      this.favorites$.next(faves.filter(jk => jk.id !== joke.id));
    } else {
      const category = joke.category[0];
      faves.push({id: joke.id, category: category});
      this.favorites$.next(faves);
    }
    this.ngforage.setItem('favourites', this.favorites$.value);
  }

}
