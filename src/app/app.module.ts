import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgForageModule, NgForageConfig } from 'ngforage';

import { ApiService } from '../app/services/api.service';

import { AppComponent } from './app.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoryComponent } from './components/category/category.component';
import { JokeComponent } from './components/joke/joke.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    CategoryComponent,
    JokeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgForageModule.forRoot({
      name: 'chuck',
      driver: [
        NgForageConfig.DRIVER_INDEXEDDB,
        NgForageConfig.DRIVER_LOCALSTORAGE
      ]
    })
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
