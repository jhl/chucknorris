import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { Favourite } from '../../models/favourite';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  public loading: boolean;
  public categories$: Observable<string[]>;
  public favourites$: Observable<Favourite[]>;
  public selectedCategory: string;

  constructor(private api: ApiService) {
    this.favourites$ = this.api.favorites$;
    this.categories$ = this.api.getCategories();
  }

  ngOnInit() {

  }

  getFavesCount(category: string, favourites: Favourite[]): number {
    return favourites.reduce( (n, fav) => {
      if (fav.category === category) {
        n++;
      }
      return n;
    }, 0);
  }

  onSelect(category: string): void {
    if (category !== this.selectedCategory) {
      this.selectedCategory = category;
    } else {
      this.selectedCategory = '';
    }
  }

}
