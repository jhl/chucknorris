import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Joke } from '../../models/joke';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnChanges, OnInit {
  @Input() category: string;

  public jokes: Joke[];
  public errorMessage: string;

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const category: SimpleChange = changes.category;
    this.jokes = [];
    this.errorMessage = '';
    if (!category.firstChange && category.currentValue) {
      this.api.getJokesByCategory(this.category, 3).subscribe( (jokes) => {
        this.jokes = jokes;
      }, (error) => {
        this.jokes = [];
        this.errorMessage = 'No jokes Today';
      });
    }
  }
}
