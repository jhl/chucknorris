import { Component, OnInit, Input } from '@angular/core';
import { Joke } from '../../models/joke';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { Favourite } from '../../models/favourite';

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.css']
})
export class JokeComponent implements OnInit {
  @Input() joke: Joke;

  public favourites$: Observable<Favourite[]>;

  constructor(private api: ApiService) {
    this.favourites$ = this.api.favorites$;
  }

  ngOnInit() {

  }

  isFavourite(joke: Joke, favourites: Favourite[]): boolean {
    return (favourites.findIndex( item => item.id === joke.id) >= 0);
  }

  makeFavourite(joke: Joke): void {
    this.api.setFavourite(joke);
  }

}
