export interface Favourite {
  id: string;
  category?: string;
}
